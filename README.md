### Requirements

To compile:
* JDK 1.8.
* Maven 3.

To run:
* JDK 1.8 or JRE 1.8.
* Assumes 1920 x 1080 resolution screenshots (otherwise coordinates need to be adjusted).

### How to compile

Run `mvn clean compile assembly:single` command from main project directory.

### How to run

Run `java "-Dscreenshot.dir=SCREEN_PATH" -jar target/wc3-games-parser-1.0-SNAPSHOT-jar-with-dependencies.jar` command from main project directory.

Where SCREEN_PATH variable is set to path of directory containing screenshots from Warcraft III, e.g. `C:/Users/user/Documents/Warcraft III/ScreenShots`.

### How it works

Application will run until explicitly closed, or until an error is encountered.

Following actions are executed in while loop:
* Waits until new file is added to SCREEN_PATH.
* Attempts to read file as a PNG image.
* Crops and splits image into 13 separate files (temporarily saved to `SCREEN_PATH/../Cropped` directory).
* Performs OCR for all cropped images (should take around 500ms on a modern processor).
* Saves text file into `SCREEN_PATH/../Text` directory with the same file name as original image, but with an extension changed from `png` to `txt`.
* Deletes all processed images.

Logs are saved into `logs/wc3-game-list-ocr.log` file (path relative to directory from which `java` command was executed).

### Example of text file with list of games

```
I[eu]|6ameofmmes(I//IZ)


[eu] Survival Chaos 3.1 New Vers (7/8)


[eu] Green TD!! rmk3 blue or teal (10/10)


[eu] Wc Mauuuulzzz !<3 (7/14)


[eu] civ wars good version ! (4/8)


[eu] pokemon defense (3/12)


[eu] 974 (4/4)


[krl - (5/7)


[eu] PMP Pimp my Peon (1/12)


[eu] rise of a realm zombie mode (4/11)


[use] the world rpg (3/12)


[eu] Angel arena eclipse (8/12)


[usw] DOTA APEMSO USA ONLY PROS! (2/10)



```

You should take note that some rows may contain:
* Empty line.
* Incorrectly parsed game name - happens approximately 15% of time.