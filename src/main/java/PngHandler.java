import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PngHandler {

    private static final int START_X = 306;
    private static final int END_X = 900;
    private static final int START_Y = 178;
    private static final int DIFF_Y = 36;
    private static final int NUM_SPLIT = 13;

    private Logger log = Logger.getLogger(getClass().getSimpleName());

    public File[] splitImage(String imagePath) throws IOException {
        BufferedImage image = ImageIO.read(new File(imagePath));
        List<File> result = new ArrayList<>();

        int currentY = START_Y;
        for (int i = 0; i < NUM_SPLIT; i++) {
            BufferedImage cropped = cropImage(image, currentY);
            result.add(saveImage(imagePath, cropped, i));

            currentY += DIFF_Y;
        }

        deleteImage(imagePath);
        return result.toArray(new File[]{});
    }

    private BufferedImage cropImage(BufferedImage image, int currentY) {
        return image.getSubimage(
                START_X,
                currentY,
                END_X - START_X,
                DIFF_Y);
    }

    private File saveImage(String imagePath, BufferedImage image, int id) throws IOException {
        String fileName = new File(imagePath).getName().replace(".png", "_" + id + ".png");
        File directory = new File(imagePath).getParentFile().getParentFile();
        File croppedFile = new File(directory.getAbsolutePath() + "\\\\Cropped\\\\" + fileName);

        log.debug("Saving cropped image: " + croppedFile);
        FileOutputStream fos = new FileOutputStream(croppedFile);
        ImageIO.write(image, "png", fos);
        fos.close();

        return croppedFile;
    }

    private void deleteImage(String imagePath) {
        log.debug("Deleting file: " + imagePath);
        new File(imagePath).delete();
    }
}