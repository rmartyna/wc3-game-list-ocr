import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class FileWatcher {

    private Path path;

    private OcrScheduler ocrScheduler;

    private Logger log = Logger.getLogger(getClass().getSimpleName());

    public FileWatcher(String screensDir, OcrScheduler ocrScheduler) {
        this.path = new File(screensDir).toPath();
        this.ocrScheduler = ocrScheduler;
    }

    public void run() {
        FileSystem fs = path.getFileSystem();

        log.debug("Starting to watch '" + path + "' for new files...");
        try (WatchService service = fs.newWatchService()) {
            path.register(service, StandardWatchEventKinds.ENTRY_CREATE);
            takeKeys(service);
        } catch (Exception e) {
            log.error("",e);;
        }
    }

    private void takeKeys(WatchService service) throws InterruptedException {
        while (true) {
            WatchKey key = service.take();
            try {
                for (WatchEvent<?> watchEvent : key.pollEvents()) {
                    handleWatchEvent(watchEvent);
                }
            } catch(Exception e) {
                log.error("",e);;
            }

            if (!key.reset()) {
                break;
            }
        }
    }

    private void handleWatchEvent(WatchEvent<?> watchEvent) throws IOException {
        WatchEvent.Kind<?> kind = watchEvent.kind();
        if (kind != StandardWatchEventKinds.ENTRY_CREATE) {
            log.debug("Ignoring wrong event: " + kind);
            return;
        }

        Path filePath = ((WatchEvent<Path>) watchEvent).context();
        String filePathString = path.toFile().getAbsolutePath() + "\\\\" + filePath.toFile().getName();
        ocrScheduler.schedule(filePathString);
    }

}
