import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class OcrScheduler implements Runnable {

    private static final int SLEEP_TIME = 1000;

    private static final int WAIT_BEFORE_PROCESS = 1000;

    private Map<String, Date> toProcess = new ConcurrentHashMap<>();

    private OcrLibrary library;

    private PngHandler pngHandler;

    private Logger log = Logger.getLogger(getClass().getSimpleName());

    public OcrScheduler(OcrLibrary library, PngHandler pngHandler) {
        this.library = library;
        this.pngHandler = pngHandler;
    }

    @Override
    public void run() {
        while(true) {
            try {
                processReadyFiles();
                Thread.sleep(SLEEP_TIME);
            } catch (Exception e) {
                log.error("", e);
            }
        }
    }

    private void processReadyFiles() throws IOException {
        Set<String> toRemove = new HashSet<>();

        Date now = new Date();
        log.debug(toProcess.size() + " files waiting to be processed.");
        for(Map.Entry<String, Date> entry : toProcess.entrySet()) {
            if (now.getTime() - entry.getValue().getTime() > WAIT_BEFORE_PROCESS) {
                toRemove.add(entry.getKey());
            }
        }

        log.debug(toRemove.size() + " will be processed now");
        for(String filePathString : toRemove) {
            toProcess.remove(filePathString);

            String result = ocrImage(filePathString);
            new File(filePathString).delete();
            saveText(new File(filePathString), result);
        }
    }

    public void schedule(String filePathString) {
        log.debug("New file scheduler for OCR: " + filePathString);
        toProcess.put(filePathString, new Date());
    }

    private String ocrImage(String filePathString) throws IOException {
        File[] files = pngHandler.splitImage(filePathString);
        StringBuilder imageText = new StringBuilder();
        for(int i = 0; i < files.length; i++) {
            try {
                String text = library.getText(files[i].getAbsolutePath());
                imageText.append(text);
                imageText.append(System.lineSeparator());
            } catch (Exception e) {
                log.error("", e);
            }
        }

        for(File file : files) {
            log.debug("Deleting file: " + file);
            file.delete();
        }

        return imageText.toString();
    }

    private void saveText(File file, String text) {
        String textFileName = file.getName().replace("png", "txt");
        File directory = file.getParentFile().getParentFile();
        File textFilePath = new File(directory.getAbsolutePath() + "\\\\Text\\\\" + textFileName);

        log.debug("Saving file '" + textFilePath + "' with text: " + text);

        try(PrintWriter out = new PrintWriter(textFilePath)) {
            out.print(text);
        } catch(IOException e) {
            log.error("", e);
        }
    }
}
