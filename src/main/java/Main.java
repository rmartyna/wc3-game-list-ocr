import org.apache.log4j.Logger;

import java.io.File;

public class Main {

    private static final String PATH_PROPERTY = "screenshot.dir";

    private static final Logger log = Logger.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        String screenPath = getScreenPath();
        initDirs(screenPath);

        OcrLibrary library = new OcrLibrary();
        try {
            library.init();
            OcrScheduler ocrScheduler = new OcrScheduler(library, new PngHandler());
            new Thread(ocrScheduler).start();

            new FileWatcher(
                    screenPath,
                    ocrScheduler).run();
        } finally {
            library.destroy();
        }
    }

    private static String getScreenPath() {
        String path = System.getProperty(PATH_PROPERTY);
        log.debug("Screen path: " + path);
        if(path == null) {
            System.exit(1);
            return null;
        } else {
            return path;
        }
    }

    private static void initDirs(String screenPath) {
        new File(new File(screenPath).getParentFile().getAbsoluteFile() + "\\\\Cropped").mkdir();
        new File(new File(screenPath).getParentFile().getAbsoluteFile() + "\\\\Text").mkdir();
    }

}
