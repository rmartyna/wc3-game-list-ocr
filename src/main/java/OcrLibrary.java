import org.apache.log4j.Logger;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;

import java.io.IOException;

public class OcrLibrary {

    private tesseract.TessBaseAPI api;

    private Logger log = Logger.getLogger(getClass().getSimpleName());

    public OcrLibrary() {
        api = new tesseract.TessBaseAPI();
    }

    public void init() {
        log.debug("Initializing OCR library..,");
        if (api.Init(".", "ENG") != 0) {
            throw new RuntimeException("Could not initialize OCR library");
        }
        log.debug("Library initialized successfully");
    }

    public void destroy() {
        log.debug("Cleaning OCR library...");
        if(api != null) {
            api.End();
        }
    }

    public String getText(String filePath) throws IOException {
        BytePointer byteText = null;
        lept.PIX byteImage = null;

        try {
            byteImage = lept.pixRead(filePath);
            if(byteImage == null) {
                throw new IOException("Could not read image: " + filePath);
            }

            log.debug("OCR starting for image: " + filePath);

            api.SetImage(byteImage);
            byteText = api.GetUTF8Text();
            String result = byteText.getString();

            log.debug("OCR finished for image: " + filePath + ", text: " + result);
            return result;
        } finally {
            if(byteText != null) {
                byteText.deallocate();
            }
            if(byteImage != null) {
                lept.pixDestroy(byteImage);
            }
        }
    }
}
